from sys import stdout as o
from comparableClass import Comparable
from mergesort import mergesort
from binary_search import binarySearchRecursive, binarySearchIteration
import random as r

if __name__ == "__main__":
    
    print("\nRandom list of comparable objects:")
    arr = []
    for i in range(0,r.randint(0,20)):
        x = r.randint(0,100)
        arr.append(Comparable(x))
        o.write("%d " % x)

    print("\nSorted with mergesort:")
    arr = mergesort(arr)
    for x in arr:
        o.write("%d " % x.numero)

    print("\nLet's search!")
    for i in range(0,10):
        x = r.randint(0,100)
        print("(Recursive) Dicotomic search of value %d: index %d" % (x, binarySearchRecursive(arr, Comparable(x))))
        print("(Iteration) Dicotomic search of value %d: index %d" % (x, binarySearchIteration(arr, Comparable(x))))
