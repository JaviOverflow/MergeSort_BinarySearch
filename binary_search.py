def binarySearchRecursive(li, obj):
    """ Search the object 'obj' at the list 'li' and return the index or -1 if
    not found.
        IMPORTANT: objects must implement __eq__() and __lt__() functions. """
    # If list is empty, return -1
    if not li:
        return -1

    # Else, look for the object
    low = 0
    high = len(li) - 1
    return privateBinSearch(li, low, high, obj)
"""
# privateBinSearch two comparisons based
def privateBinSearch(li, low, high, obj):
    
    if high < low:
        return -1

    mid = (low + high) // 2
    if obj == li[mid]:
        return mid
    elif obj < li[mid]:
        return privateBinSearch(li, low, mid - 1, obj)
    else:
        return privateBinSearch(li, mid + 1, high, obj)
"""

# privateBinSearch single comparison based
def privateBinSearch(li, low, high, obj):

    if high == low:
        return high if li[high] == obj else -1

    mid = (low + high) // 2
    if li[mid] < obj:
        return privateBinSearch(li, mid + 1, high, obj)
    else:
        return privateBinSearch(li, low, mid, obj)

# privateBinSearch single comparison based
def binarySearchIteration(li, obj):
    """ Search the object 'obj' at the list 'li' and return the index or -1 if
    not found.
        IMPORTANT: objects must implement __ne__() and __lt__() functions. """
    # If list is empty, return -1
    if not li:
        return -1

    # Else, look for the object
    low = 0
    high = len(li) - 1
    
    while high != low:
        mid = (low + high) // 2
        if li[mid] < obj:
            low = mid + 1
        else:
            high = mid
    return high if li[high] == obj else -1
