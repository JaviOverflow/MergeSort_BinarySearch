class Comparable:
    """ Test class that implements the base functions used to compare in other
    methods """
    def __init__(self, numero):
        self.numero = numero

    def __lt__(self, entero):
        """ Method called when using '<' operator between objects """
        if not isinstance(entero, type(self)):
            raise AttributeError
        return self.numero < entero.numero
    def __eq__(self, entero):
        """ Method called when using '==' operator between objects """
        if not isinstance(entero, type(self)):
            raise AttributeError
        return self.numero == entero.numero
    def __ne__(self, entero):
        """ Method called when using '!=' operator between objects """
        if not isinstance(entero, type(self)):
            raise AttributeError
        return self.numero != entero.numero
