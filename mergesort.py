# -*- coding: latin-1 -*-

def mergesort(array):
    """ Sorts a list of objects using mergesort algorithm.
    Throws AtributeError in case list is not homogeneous or objects doesn't 
    implement built-in method __lt__()"""
    
    # Trivial case
    if len(array) <= 1:
        return array
    # Non-Trivial case
    else:
        middle = len(array) / 2
        
        left = array[:middle]
        right = array[middle:]
        
        # Recursive call with left and right half of array
        left = mergesort(left)
        right = mergesort(right)
        
        # If biggest left list item is lower than lowest right list item,
        #   we can append lists.
        if left[-1] < right[0]:
            return left + right
        # Else we merge checking items.
        else:
            return merge(left, right)


def merge(left, right):
    """ Merge two sorted arrays of objects using built-in method __lt__() of the
    objects in the list.
    Throws AtributeError in case list is not homogeneous or objects doesn't implement
    built-in method __lt__()"""

    result = [] #Result array

    # Compare items in both list and append the lowest to result array
    while len(left)>0 and len(right)>0:
        if left[0] < right[0]:
            result.append(left[0])
            left.pop(0)
        else:
            result.append(right[0])
            right.pop(0)

    # Append the rest of the non-empty list
    if len(left) > 0:
        result += left
    else:
        result += right

    return result
